import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocsService {

  url = environment.URL;

  constructor(private _http: HttpClient) { }

  obtenerArchivos() {
    const url = `${this.url}/docs/archivos`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  subirArchivo(data: any) {
    const url = `${this.url}/docs/subirDoc`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }
}
