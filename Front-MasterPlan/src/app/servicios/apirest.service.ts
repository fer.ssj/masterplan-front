import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApirestService {
  url = environment.URL;

  constructor(private _http: HttpClient) { }

  login(data: any) {
    const url = `${this.url}/usuario/login`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  verificarToken() {
    const url = `${this.url}/usuario/verificarToken`;
    return this._http.get(url);
  }

  obtenerUsuarios() {
    const url = `${this.url}/usuario/obtenerUsuarios`;
    return this._http.get(url);
  }

  obtenerIDUsuario(email: any) {
    const url = `${this.url}/usuario/obtenerIdUsuario/${email}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  nuevoRoleUsuario(data: any) {
    const url = `${this.url}/usuario/nuevoRole`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerRolesUsuario(id: any) {
    const url = `${this.url}/usuario/obtenerRolesUsuario/${id}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  eliminarRoleUsuario(data: any) {
    const url = `${this.url}/usuario/eliminarRole`;
    return this._http.delete(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json"),
      body: data
    });
  }

  obtenerUsuarioSesion(email: any) {
    const url = `${this.url}/usuario/obtenerDatosUsuario/${email}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  crearUsuario(data: any) {
    const url = `${this.url}/usuario/nuevoUsuario`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  eliminarUsuario(id: any) {
    const url = `${this.url}/usuario/eliminarUsuario/${id}`;
    return this._http.patch(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarUsuario(data: any) {
    const url = `${this.url}/usuario/actualizarUsuario`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarUsuarioEmail(data: any) {
    const url = `${this.url}/usuario/actualizarUsuarioEmail`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarPFP(data: any) {
    const url = `${this.url}/usuario/cambiarPFP`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerActividadesUsuario(idUsuario: any) {
    const url = `${this.url}/usuario/obtenerActividades/${idUsuario}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }
  obtenerIndicadoresUsuario(idUsuario: any) {
    const url = `${this.url}/usuario/obtenerIndicadores/${idUsuario}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }
}
