import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReunionesService {
  url = environment.URL;

  constructor(private _http: HttpClient) { }

  obtenerIDReunion(id: any) {
    const url = `${this.url}/reunion/obtenerReuniones/${id}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerIDReuniones(id: any) {
    const url = `${this.url}/reunion/obtenerReunionesReunion/${id}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerDatosReunion(id: any) {
    const url = `${this.url}/reunion/obtenerDatosReuniones/${id}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  crearReunion(data: any) {
    const url = `${this.url}/reunion/nuevaReunion`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarNombreReunion(data: any) {
    const url = `${this.url}/reunion/actualizarNombreReunion`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarStatusReunion(data: any) {
    const url = `${this.url}/reunion/actualizarStatusReunion`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  nuevaReunionIniciada(data: any) {
    const url = `${this.url}/reunion/nuevaReunionIniciada`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerReunionDeIniciada(id: any) {
    const url = `${this.url}/reunion/obtenerIDReunionIniciada/${id}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerTiempoReunionIniciada(idReunionIniciada: any) {
    const url = `${this.url}/reunion/obtenerTiempoReunionIniciada/${idReunionIniciada}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerIDUltimaReunion(id: any) {
    const url = `${this.url}/reunion/obtenerUltimaReunionIniciada/${id}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarMinutoReunionIniciada(data: any) {
    const url = `${this.url}/reunion/actualizarMinutoReunion`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarMinutosPaginas(data: any) {
    const url = `${this.url}/reunion/actualizarMinutosPaginas`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  borrarIntegrantes(id: any) {
    const url = `${this.url}/reunion/eliminarIntegrantes/${id}`;
    return this._http.delete(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  integrantes(data: any) {
    const url = `${this.url}/reunion/integrantes`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  paginas(data: any) {
    const url = `${this.url}/reunion/paginas`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerPaginas(idReunion: any) {
    const url = `${this.url}/reunion/obtenerPaginas/${idReunion}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  borrarPaginas(id: any) {
    const url = `${this.url}/reunion/eliminarPaginas/${id}`;
    return this._http.delete(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  crearIndicador(data: any) {
    const url = `${this.url}/reunion/nuevoIndicador`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  editarIndicador(data: any) {
    const url = `${this.url}/reunion/editarIndicador`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  archivarIndicador(data: any) {
    const url = `${this.url}/reunion/archivarIndicador/`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  crearSeguimientoIndicador(data: any) {
    const url = `${this.url}/reunion/nuevoSeguimientoIndicador`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarSeguimientoIndicador(data: any) {
    const url = `${this.url}/reunion/actualizarSeguimientoIndicador`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerIndicadores(idReunion: any) {
    const url = `${this.url}/reunion/obtenerMeasurables/${idReunion}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerSeguimiento(idMeasurable: any) {
    const url = `${this.url}/reunion/obtenerCumplimientoMeasurables/${idMeasurable}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerSeguimientoFechas(data: any) {
    const url = `${this.url}/reunion/obtenerCumplimientoFechasMeasurables/${data.idMeasurable}/${data.fechaMeasurable}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerMetas(idReunion: any) {
    const url = `${this.url}/reunion/obtenerMetas/${idReunion}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  crearMeta(data: any) {
    const url = `${this.url}/reunion/nuevaMeta`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerPlaneadoMes(mesAnioPlan: any, idGoal: any) {
    const url = `${this.url}/reunion/obtenerPlaneadoMes/${mesAnioPlan}/${idGoal}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  guardarPlaneadoMes(data: any) {
    const url = `${this.url}/reunion/guardarPlaneadoMes`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarPlaneadoMes(data: any) {
    const url = `${this.url}/reunion/actualizarPlaneadoMes`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerSeguimientoRocas(fechaGoal: any, idGoal: any) {
    const url = `${this.url}/reunion/obtenerSeguimientoRocas/${fechaGoal}/${idGoal}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  guardarSeguimiento(data: any) {
    const url = `${this.url}/reunion/guardarSeguimiento`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarSeguimiento(data: any) {
    const url = `${this.url}/reunion/actualizarSeguimiento`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarRealMes(data: any) {
    const url = `${this.url}/reunion/sumarRealMes`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarRealMesModificado(data: any) {
    const url = `${this.url}/reunion/actualizarRealMes`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarPorcentaje(data: any) {
    const url = `${this.url}/reunion/sumarPorcentaje`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarPorcentajeModificado(data: any) {
    const url = `${this.url}/reunion/actualizarPorcentaje`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerComentarios(fechaGoal: any, idGoal: any) {
    const url = `${this.url}/reunion/obtenerComentario/${fechaGoal}/${idGoal}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  agregarComentario(data: any) {
    const url = `${this.url}/reunion/agregarComentario`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  editarMeta(data: any) {
    const url = `${this.url}/reunion/editarMeta`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  archivarMeta(data: any) {
    const url = `${this.url}/reunion/archivarMeta`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  crearProblema(data: any) {
    const url = `${this.url}/reunion/nuevoProblema`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerActividades(idReunion: any) {
    const url = `${this.url}/reunion/obtenerActividades/${idReunion}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  crearActividad(data: any) {
    const url = `${this.url}/reunion/nuevaActividad`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarTituloActividad(data: any) {
    const url = `${this.url}/reunion/actualizarTituloActividad`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarStatusActividad(data: any) {
    const url = `${this.url}/reunion/actualizarStatusToDo`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarFechaActividad(data: any) {
    const url = `${this.url}/reunion/actualizarFechaToDo`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarUsuarioActividad(data: any) {
    const url = `${this.url}/reunion/actualizarUsuarioToDo`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarStatusActividadNull(data: any) {
    const url = `${this.url}/reunion/actualizarStatusActividadNull`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerAsuntos(idReunion: any) {
    const url = `${this.url}/reunion/obtenerAsuntos/${idReunion}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerAsuntosReunionIniciada(idReunionIniciada: any) {
    const url = `${this.url}/reunion/obtenerAsuntosResueltos/${idReunionIniciada}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  crearAsunto(data: any) {
    const url = `${this.url}/reunion/nuevoAsunto`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarTituloAsunto(data: any) {
    const url = `${this.url}/reunion/actualizarTituloAsunto`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarUsuarioAsunto(data: any) {
    const url = `${this.url}/reunion/actualizarUsuarioAsunto`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerProblemas(idReunion: any) {
    const url = `${this.url}/reunion/obtenerProblemas/${idReunion}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerProblemasReunionIniciada(idReunionIniciada: any) {
    const url = `${this.url}/reunion/obtenerProblemasResueltos/${idReunionIniciada}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarTituloProblema(data: any) {
    const url = `${this.url}/reunion/actualizarTituloProblema`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarStatusAsunto(data: any) {
    const url = `${this.url}/reunion/actualizarStatusAsunto`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarStatusAsuntoNull(data: any) {
    const url = `${this.url}/reunion/actualizarStatusAsuntoNull`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarStatusProblema(data: any) {
    const url = `${this.url}/reunion/actualizarStatusIssue`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarUsuarioProblema(data: any) {
    const url = `${this.url}/reunion/actualizarUsuarioIssue`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  numerarProblema(data: any) {
    const url = `${this.url}/reunion/numerarIssue`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  quitarNumeroProblema(data: any) {
    const url = `${this.url}/reunion/quitarNumeroIssue`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarStatusProblemaNull(data: any) {
    const url = `${this.url}/reunion/actualizarStatusProblemaNull`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  enviarCorreo(variables: any) {
    const url = `${this.url}/reunion/enviarCorreo`;
    return this._http.post(url, variables, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

}
