import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProyectosService {


  url = environment.URL;

  constructor(private _http: HttpClient) { }

  obtenerRocas(id: any) {
    const url = `${this.url}/proyecto/obtenerRocas/${id}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  nuevaRoca(data: any) {
    const url = `${this.url}/proyecto/nuevaRoca`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  editarRoca(data: any) {
    const url = `${this.url}/proyecto/editarRoca`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  archivarRoca(data: any) {
    const url = `${this.url}/proyecto/archivarRoca`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerActividades(id: any) {
    const url = `${this.url}/proyecto/obtenerActividades/${id}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerActividadesSecundarias(id: any) {
    const url = `${this.url}/proyecto/obtenerActividadesSecundarias/${id}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  editarActividad(data: any) {
    const url = `${this.url}/proyecto/editarActividad`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  archivarActividad(data: any) {
    const url = `${this.url}/proyecto/archivarActividad`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerStatus(fecha: any, idActividad: any) {
    const url = `${this.url}/proyecto/verificarDia/${fecha}/${idActividad}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  nuevaActividad(data: any) {
    const url = `${this.url}/proyecto/nuevaActividad`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  nuevoPlan(data: any) {
    const url = `${this.url}/proyecto/planearActividad`;
    return this._http.post(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  actualizarStatusActividad(data: any) {
    const url = `${this.url}/proyecto/actualizarStatusActividad`;
    return this._http.patch(url, data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  obtenerColaboradores(id: any) {
    const url = `${this.url}/proyecto/obtenerColaboradores/${id}`;
    return this._http.get(url, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }
}