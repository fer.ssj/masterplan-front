import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { ROUTES } from './app.route';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgSelectModule } from "@ng-select/ng-select";
import { OrganizationChartModule } from 'primeng/organizationchart';
import { TreeModule } from 'primeng/tree';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPanZoomModule } from 'ngx-panzoom';




import { AppComponent } from './app.component';
import { LoginComponent } from './componentes/login/login.component';
import { MenuPrincipalComponent } from './componentes/compartidos/menu-principal/menu-principal.component';
import { UsuariosComponent } from './componentes/superAdministrador/usuarios/usuarios.component';
import { TokenInterceptorInterceptor } from './token-interceptor.interceptor';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './componentes/compartidos/navbar/navbar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EditarPerfilComponent } from './componentes/compartidos/editar-perfil/editar-perfil.component';
import { ReunionesComponent } from './componentes/compartidos/reuniones/reuniones.component';
import { CrearReunionComponent } from './componentes/superAdministrador/crear-reunion/crear-reunion.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ReunionComponent } from './componentes/compartidos/reunion/reunion.component';
import { RompehielosComponent } from './componentes/compartidos/paginas/rompehielos/rompehielos.component';
import { IndicadoresComponent } from './componentes/compartidos/paginas/indicadores/indicadores.component';
import { MetasComponent } from './componentes/compartidos/paginas/metas/metas.component';
import { AsuntosComponent } from './componentes/compartidos/paginas/asuntos/asuntos.component';
import { ActividadesComponent } from './componentes/compartidos/paginas/actividades/actividades.component';
import { ProblemasComponent } from './componentes/compartidos/paginas/problemas/problemas.component';
import { CalificacionComponent } from './componentes/compartidos/paginas/calificacion/calificacion.component';
import { ConclusionComponent } from './componentes/compartidos/paginas/conclusion/conclusion.component';
import { ExternoComponent } from './componentes/compartidos/paginas/externo/externo.component';
import { ReunionIniciadaComponent } from './componentes/compartidos/reunion-iniciada/reunion-iniciada.component';
import { DocsComponent } from './componentes/compartidos/docs/docs.component';
import { TodasActividadesComponent } from './componentes/compartidos/todas-actividades/todas-actividades.component';
import { VtoComponent } from './componentes/compartidos/vto/vto.component';
import { PersonasComponent } from './componentes/compartidos/personas/personas.component';
import { EditarReunionComponent } from './componentes/superAdministrador/editar-reunion/editar-reunion.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuPrincipalComponent,
    UsuariosComponent,
    NavbarComponent,
    EditarPerfilComponent,
    ReunionesComponent,
    CrearReunionComponent,
    ReunionComponent,
    RompehielosComponent,
    IndicadoresComponent,
    MetasComponent,
    AsuntosComponent,
    ActividadesComponent,
    ProblemasComponent,
    CalificacionComponent,
    ConclusionComponent,
    ExternoComponent,
    ReunionIniciadaComponent,
    DocsComponent,
    TodasActividadesComponent,
    VtoComponent,
    PersonasComponent,
    EditarReunionComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    CKEditorModule,
    NgSelectModule,
    OrganizationChartModule,
    TreeModule,
    NgxPanZoomModule,
    NgMultiSelectDropDownModule.forRoot(),
    RouterModule.forRoot(ROUTES),
  ],
  providers: [HttpClientModule, { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
