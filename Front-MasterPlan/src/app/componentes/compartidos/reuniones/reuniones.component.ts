import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';

@Component({
  selector: 'app-reuniones',
  templateUrl: './reuniones.component.html',
  styleUrls: ['./reuniones.component.css']
})
export class ReunionesComponent implements OnInit {

  roleUsuario: any
  tokenPayload: any;
  reuniones: any = [];
  idUsuario: any
  respDatosReunion: any

  constructor(private _service: ApirestService, private _reuniones: ReunionesService, private _router: Router) { }


  ngOnInit(): void {
    this.token();
    this.obtenerReuniones();
  }

  token() {
    const token: any = localStorage.getItem('token')
    this.tokenPayload = jwt_decode(token);
    this.roleUsuario = this.tokenPayload.roleUsuario;
  }

  async obtenerReuniones() {
    try {
      this.idUsuario = await this._service.obtenerIDUsuario(this.tokenPayload.emailUsuario).toPromise();
      const idUsuario = this.idUsuario[0].idUsuario;
      const respReuniones = await this._reuniones.obtenerIDReunion(idUsuario).toPromise();
      this.reuniones = respReuniones;
      function compare(a: any, b: any): number {
        return a.idReunion.split("R")[1] - b.idReunion.split("R")[1];
      }
      this.reuniones.sort(compare);

      for (let reus = 0; reus < this.reuniones.length; reus++) {
        try {
          const respDatosReunion: any = await this._reuniones.obtenerDatosReunion(this.reuniones[reus].idReunion).toPromise();
          if (respDatosReunion && respDatosReunion.length > 0) {
            const nombresUsuario = respDatosReunion.map((datos: any) => datos.nombreUsuario);
            const imgsUsuarios = respDatosReunion.map((datos: any) => datos.imagenUsuario);
            const apellidosUsuario = respDatosReunion.map((datos: any) => datos.apellidoUsuario);
            this.reuniones[reus] = {
              idReunion: this.reuniones[reus].idReunion,
              statusReunion: respDatosReunion[0].statusReunion || 'Valor predeterminado',
              nombreReunion: respDatosReunion[0].nombreReunion,
              nombresParticipantes: nombresUsuario,
              imagenesUsuarios: imgsUsuarios,
              apellidosParticipantes: apellidosUsuario,
            };
          } else {
            console.error('El valor de respDatosReunion no está definido o es un array vacío.');
          }
        } catch (error) {
          console.log(error);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }


  async entrarReunion(idReunion: any) {
    let status: any = await this._reuniones.obtenerIDReuniones(idReunion).toPromise()
    if (status[0].statusReunion == 'Iniciada') {
      let ultimaReu: any = await this._reuniones.obtenerIDUltimaReunion(idReunion).toPromise()
      this._router.navigate(['/reunionIniciada', ultimaReu[0].idReunionIniciada]);

    } else {
      this._router.navigate(['/reunion', idReunion]);
    }
  }

  async editarReunion(idReunion: any) {
      this._router.navigate(['/editarReunion', idReunion]);
  }

}
