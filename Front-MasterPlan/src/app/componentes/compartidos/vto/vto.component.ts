import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';

@Component({
  selector: 'app-vto',
  templateUrl: './vto.component.html',
  styleUrls: ['./vto.component.css']
})
export class VtoComponent {
  roleUsuario: any
  tokenPayload: any;
  imagenUrl: string = '';

  constructor(private _service: ApirestService, private _reuniones: ReunionesService, private _router: Router, private http: HttpClient) { }

  ngOnInit(): void {
    const token: any = localStorage.getItem('token')
    this.tokenPayload = jwt_decode(token);
    this.roleUsuario = this.tokenPayload.roleUsuario;

    this.http.get<any>('http://localhost:8080/vto/obtenerUltimaImagen').subscribe(
      (response) => {
        this.imagenUrl = response.url;
      },
      (error) => {
        this.imagenUrl = '../../../../assets/img/NoImagen.png';
      }
    );
  }

  cambiarImagen(event: Event) {
    const input = event.target as HTMLInputElement;
    const file = input?.files?.[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        this.imagenUrl = reader.result as string;
      };
      reader.readAsDataURL(file);
      const formData = new FormData();
      formData.append('imagen', file);
      this.http.patch(`http://localhost:8080/vto/cambiarVTO`, formData).subscribe(
        (response) => {
        }, (error) => {
          this.imagenUrl = '../../../../assets/img/NoImagen.png';
        });
    }
  }
}
