import { Component, OnInit, ChangeDetectorRef, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';
import { format, startOfWeek, endOfWeek, parseISO, parse } from 'date-fns';
import { es } from 'date-fns/locale';
import { ReunionIniciadaComponent } from '../../reunion-iniciada/reunion-iniciada.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as ClassicEditor from 'ckeditor5-custom/build/ckeditor';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-asuntos',
  templateUrl: './asuntos.component.html',
  styleUrls: ['./asuntos.component.css']
})
export class AsuntosComponent {
  @ViewChild(ReunionIniciadaComponent) iniciadaComp: ReunionIniciadaComponent | undefined;
  @ViewChild('crearProblemaModal', { static: false }) agregarProblemaModal!: TemplateRef<any>;
  @ViewChild('crearActividadModal', { static: false }) agregarActividadModal!: TemplateRef<any>;

  idReunionIniciada: any
  idReunion: any
  asuntos: any
  seleccion = false
  index: any
  respDatosReunion: any
  participantes: any
  public Editor: any = ClassicEditor;

  problema = { idIssue: '', nombreIssue: '', detallesIssue: '', fechaIssue: '', statusIssue: '', idUsuario: '', idReunion: '', idReunionIniciada: '' }
  actividad: any = { idToDo: '', tituloToDo: '', detallesToDo: '', fechaCreadoToDo: '', dueDateToDo: '', statusToDo: '', idUsuario: [], idReunion: '', idReunionIniciada: '' }


  constructor(private _service: ApirestService, private _reunion: ReunionesService, private _router: Router, private route: ActivatedRoute, private _modal: NgbModal, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.route.params.subscribe(async params => {
      const idReunionString = params['idReunionIniciada'];
      const idNumero = parseInt(idReunionString.match(/\d+/)[0]);
      this.idReunionIniciada = 'SR' + idNumero
      const auxiliarID: any = await this._reunion.obtenerReunionDeIniciada(this.idReunionIniciada).toPromise()
      this.idReunion = auxiliarID[0].idReunion
      this.obtenerAsuntos()
      this.cdr.detectChanges()
      this.obtenerReuniones()
    });
  }

  async obtenerReuniones() {
    this.respDatosReunion = await this._reunion.obtenerDatosReunion(this.idReunion).toPromise();
    try {
      this.respDatosReunion = await this._reunion.obtenerDatosReunion(this.idReunion).toPromise();
      this.participantes = this.respDatosReunion.map((datos: any) => {
        return {
          idUsuario: datos.idUsuario,
          pfp: datos.imagenUsuario,
          nombre: datos.nombreUsuario,
          apellido: datos.apellidoUsuario,
          email: datos.emailUsuario
        };
      });
    } catch (error) {
      console.log(error);
    }
  }

  async obtenerAsuntos() {
    let todosLosAsuntos: any = await this._reunion.obtenerAsuntos(this.idReunion).toPromise();
    this.asuntos = todosLosAsuntos.filter((asunto: { statusHeadline: string; }) => asunto.statusHeadline === 'Activo');
    function compare(a: any, b: any): number {
      return a.idHeadline.split("H")[1] - b.idHeadline.split("H")[1];
    }

    this.asuntos.sort(compare);
    for (let index = 0; index < this.asuntos.length; index++) {
      this.asuntos[index].seleccionado = 'No'
    }
    this.cdr.detectChanges();
  }


  mostrarDetalles(index: number) {
    for (let index = 0; index < this.asuntos.length; index++) {
      this.asuntos[index].seleccionado = 'No'
    }
    this.asuntos[index].seleccionado='Si'
    this.seleccion = true
    this.index = index

    return this.index;
  }


  actualizarUsuario(datos: any) {
    const data = { idHeadline: this.asuntos[this.index].idHeadline, idUsuario: datos }
    this._reunion.actualizarUsuarioAsunto(data).subscribe((resp: any) => {
      this.obtenerAsuntos()
    }, err => {
      console.log(err);
    })
  }

  abrirNuevoProblemaModal(data: any) {
    this.problema.detallesIssue = "<p><i>Contexto: " + data.nombreHeadline + " </i></p>" + data.detallesHeadline
    this.problema.idUsuario = data.idUsuario
    this._modal.open(this.agregarProblemaModal);
  }

  agregarProblema() {
    if (this.problema.nombreIssue == '' || this.problema.idUsuario == '') {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this.problema.idReunion = this.idReunion
      this.problema.statusIssue = 'Activo'
      let fechaHoy = new Date();
      let fechaSolo = fechaHoy.toISOString().split('T')[0];
      this.problema.fechaIssue = fechaSolo
      this.problema.idReunionIniciada = this.idReunionIniciada
      this._reunion.crearProblema(this.problema).subscribe((resp: any) => {
        Swal.fire({
          icon: 'success',
          title: 'Problema creado exitosamente!',
          showConfirmButton: false,
          timer: 1500
        });
        this.problema = { idIssue: '', nombreIssue: '', detallesIssue: '', fechaIssue: '', statusIssue: '', idUsuario: '', idReunion: '', idReunionIniciada: '' }
        this.cerrarModal()
      }, err => {
        console.log(err);
      })
    }
  }

  abrirNuevaActividadModal(data: any) {
    this.actividad.detallesToDo = "<p><i>Contexto asunto: " + data.nombreHeadline + " </i></p>" + data.detallesHeadline
    this.actividad.idUsuario.push(data.idUsuario)
    this._modal.open(this.agregarActividadModal);
  }

  async agregarActividad() {

    let fechaHoy = new Date();
    let fechaSolo = fechaHoy.toISOString().split('T')[0];
    this.actividad.fechaCreadoToDo = fechaSolo
    if (this.actividad.tituloToDo == '' || this.actividad.dueDateToDo == '' || this.actividad.idUsuario.length < 0) {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos.',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this.actividad.idReunion = this.idReunion
      this.actividad.statusToDo = 'Activo'
      this.actividad.idReunionIniciada = this.idReunionIniciada
      for (let index = 0; index < this.actividad.idUsuario.length; index++) {
        const data = { tituloToDo: this.actividad.tituloToDo, detallesToDo: this.actividad.detallesToDo, fechaCreadoToDo: this.actividad.fechaCreadoToDo, dueDateToDo: this.actividad.dueDateToDo, statusToDo: this.actividad.statusToDo, idUsuario: this.actividad.idUsuario[index], idReunion: this.idReunion }
        await this._reunion.crearActividad(data).toPromise()
      }
      Swal.fire({
        icon: 'success',
        title: 'Se creó la actividad con éxito',
        showConfirmButton: false,
        timer: 1500
      });
      this.actividad = { idToDo: '', tituloToDo: '', detallesToDo: '', fechaCreadoToDo: '', dueDateToDo: '', statusToDo: '', idUsuario: [], idReunion: '', idReunionIniciada: '' }
      this.cerrarModal()
    }

  }

  cerrarModal() {
    this._modal.dismissAll();
  }
}
