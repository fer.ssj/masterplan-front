import { Component } from '@angular/core';

@Component({
  selector: 'app-calificacion',
  templateUrl: './calificacion.component.html',
  styleUrls: ['./calificacion.component.css']
})
export class CalificacionComponent {

  calificacionRecomendada = 0
  tAsistencia = false;
  cAsistencia = false;
  promedioAsistencia = 0;
  tRompehielos = false;
  cRompehielos = false;
  promedioRompehielos = 0;
  tIndicadores = false;
  cIndicadores = false;
  promedioIndicadores = 0;
  tRocas = false;
  cRocas = false;
  promedioRocas = 0;
  tTitulares = false;
  cTitulares = false;
  promedioTitulares = 0;
  porcentajeActividades = 0;
  tIDS = false;
  cIDS = false;
  promedioIDS = 0;

  constructor() { }

  ngOnInit() {
    this.calcularPromedioAsistencia();
    this.calcularPromedioRompehielos();
    this.calcularPromedioIndicadores();
    this.calcularPromedioRocas();
    this.calcularPromedioTitulares();
    this.calcularPromedioIDS();
    this.calificacion()
  }

  calificacion() {
    const sumaCalificaciones =
      this.promedioAsistencia +
      this.promedioRompehielos +
      this.promedioIndicadores +
      this.promedioRocas +
      this.promedioTitulares +
      this.promedioIDS +
      this.porcentajeActividades;
    this.calificacionRecomendada = Math.round(sumaCalificaciones / 7);
  }


  tiempoAsistencia() {
    this.tAsistencia = !this.tAsistencia;
    this.calcularPromedioAsistencia();
    this.calificacion()
  }

  cumplimientoAsistencia() {
    this.cAsistencia = !this.cAsistencia;
    this.calcularPromedioAsistencia();
    this.calificacion()
  }

  tiempoRompehielos() {
    this.tRompehielos = !this.tRompehielos;
    this.calcularPromedioRompehielos();
    this.calificacion()
  }

  cumplimientoRompehielos() {
    this.cRompehielos = !this.cRompehielos;
    this.calcularPromedioRompehielos();
    this.calificacion()
  }

  tiempoIndicadores() {
    this.tIndicadores = !this.tIndicadores;
    this.calcularPromedioIndicadores();
    this.calificacion()
  }

  cumplimientoIndicadores() {
    this.cIndicadores = !this.cIndicadores;
    this.calcularPromedioIndicadores();
    this.calificacion()
  }

  tiempoRocas() {
    this.tRocas = !this.tRocas;
    this.calcularPromedioRocas();
    this.calificacion()
  }

  cumplimientoRocas() {
    this.cRocas = !this.cRocas;
    this.calcularPromedioRocas();
    this.calificacion()
  }

  tiempoTitulares() {
    this.tTitulares = !this.tTitulares;
    this.calcularPromedioTitulares();
    this.calificacion()
  }

  cumplimientoTitulares() {
    this.cTitulares = !this.cTitulares;
    this.calcularPromedioTitulares();
    this.calificacion()
  }



  tiempoIDS() {
    this.tIDS = !this.tIDS;
    this.calcularPromedioIDS();
    this.calificacion()
  }

  cumplimientoIDS() {
    this.cIDS = !this.cIDS;
    this.calcularPromedioIDS();
    this.calificacion()
  }

  calcularPromedioAsistencia() {
    const totalChecks = (this.tAsistencia ? 1 : 0) + (this.cAsistencia ? 1 : 0);
    this.promedioAsistencia = totalChecks === 2 ? 100 : totalChecks === 1 ? 50 : 0;
  }

  calcularPromedioRompehielos() {
    const totalChecks = (this.tRompehielos ? 1 : 0) + (this.cRompehielos ? 1 : 0);
    this.promedioRompehielos = totalChecks === 2 ? 100 : totalChecks === 1 ? 50 : 0;
  }

  calcularPromedioIndicadores() {
    const totalChecks = (this.tIndicadores ? 1 : 0) + (this.cIndicadores ? 1 : 0);
    this.promedioIndicadores = totalChecks === 2 ? 100 : totalChecks === 1 ? 50 : 0;
  }

  calcularPromedioRocas() {
    const totalChecks = (this.tRocas ? 1 : 0) + (this.cRocas ? 1 : 0);
    this.promedioRocas = totalChecks === 2 ? 100 : totalChecks === 1 ? 50 : 0;
  }

  calcularPromedioTitulares() {
    const totalChecks = (this.tTitulares ? 1 : 0) + (this.cTitulares ? 1 : 0);
    this.promedioTitulares = totalChecks === 2 ? 100 : totalChecks === 1 ? 50 : 0;
  }

  calcularPromedioIDS() {
    const totalChecks = (this.tIDS ? 1 : 0) + (this.cIDS ? 1 : 0);
    this.promedioIDS = totalChecks === 2 ? 100 : totalChecks === 1 ? 50 : 0;
  }
}
