import { Component, OnInit, ChangeDetectorRef, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';
import { ReunionIniciadaComponent } from '../../reunion-iniciada/reunion-iniciada.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as ClassicEditor from 'ckeditor5-custom/build/ckeditor';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-problemas',
  templateUrl: './problemas.component.html',
  styleUrls: ['./problemas.component.css']
})
export class ProblemasComponent implements OnInit {
  @ViewChild(ReunionIniciadaComponent) iniciadaComp: ReunionIniciadaComponent | undefined;
  @ViewChild('crearActividadModal', { static: false }) agregarActividadModal!: TemplateRef<any>;

  idReunionIniciada: any
  idReunion: any
  problemas: any
  seleccion = false
  index: any
  respDatosReunion: any
  participantes: any
  indiceSeleccionado: number = 0
  public Editor: any = ClassicEditor;

  problema = { idIssue: '', nombreIssue: '', detallesIssue: '', fechaIssue: '', statusIssue: '', idUsuario: '', idReunion: '' }
  actividad: any = { idToDo: '', tituloToDo: '', detallesToDo: '', dueDateToDo: '', statusToDo: '', idUsuario: [], idReunion: '', idReunionIniciada: '' }


  constructor(private _service: ApirestService, private _reunion: ReunionesService, private _router: Router, private route: ActivatedRoute, private _modal: NgbModal) { }

  ngOnInit() {
    this.route.params.subscribe(async params => {
      const idReunionString = params['idReunionIniciada'];
      const idNumero = parseInt(idReunionString.match(/\d+/)[0]);
      this.idReunionIniciada = 'SR' + idNumero
      const auxiliarID: any = await this._reunion.obtenerReunionDeIniciada(this.idReunionIniciada).toPromise()
      this.idReunion = auxiliarID[0].idReunion
      this.obtenerProblemas()
      this.obtenerReuniones()
    });
  }

  async obtenerReuniones() {
    try {
      this.respDatosReunion = await this._reunion.obtenerDatosReunion(this.idReunion).toPromise();
      this.participantes = this.respDatosReunion.map((datos: any) => {
        return {
          idUsuario: datos.idUsuario,
          pfp: datos.imagenUsuario,
          nombre: datos.nombreUsuario,
          apellido: datos.apellidoUsuario,
          email: datos.emailUsuario
        };
      });
    } catch (error) {
      console.error('Error al obtener reuniones:', error);
      // Agrega aquí el manejo del error (p. ej., mostrar un mensaje al usuario)
    }
  }

  async obtenerProblemas() {
    this.problemas = await this._reunion.obtenerProblemas(this.idReunion).toPromise();
    this.problemas = this.problemas.filter((m: { statusIssue: string; }) => m.statusIssue === 'Activo');
    if (this.problemas.length > 0) {
      this.indiceSeleccionado = Math.max(...this.problemas.map((issue: { numeroIssue: any; }) => issue.numeroIssue));
    }
    for (let index = 0; index < this.problemas.length; index++) {
      this.problemas[index].seleccionado = 'No'
    }
  }

  mostrarDetalles(index: number) {
    for (let index = 0; index < this.problemas.length; index++) {
      this.problemas[index].seleccionado = 'No'
    }
    this.problemas[index].seleccionado='Si'
    this.seleccion = true
    this.index = index

    return this.index;
  }

  actualizarStatus(datos: any) {
    this._reunion.actualizarStatusProblema(datos).subscribe((resp: any) => {
      this.obtenerProblemas()
    }, err => {
      console.log(err);
    })
  }

  actualizarUsuario(datos: any) {
    const data = { idIssue: this.problemas[this.index].idIssue, idUsuario: datos }
    this._reunion.actualizarUsuarioProblema(data).subscribe((resp: any) => {
      this.obtenerProblemas()
    }, err => {
      console.log(err);
    })
  }


  abrirNuevaActividadModal(data: any) {
    this.actividad.detallesToDo = "<p><i>Contexto problema: " + data.nombreIssue + " </i></p>" + data.detallesIssue
    this.actividad.idUsuario.push(data.idUsuario)
    this._modal.open(this.agregarActividadModal);
  }

  async agregarActividad() {
    if (this.actividad.tituloToDo == '' || this.actividad.dueDateToDo == '' || this.actividad.idUsuario.length < 0) {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos.',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this.actividad.idReunion = this.idReunion
      this.actividad.statusToDo = 'Activo'
      this.actividad.idReunionIniciada = this.idReunionIniciada
      for (let index = 0; index < this.actividad.idUsuario.length; index++) {
        const data = { tituloToDo: this.actividad.tituloToDo, detallesToDo: this.actividad.detallesToDo, fechaCreadoToDo: this.actividad.fechaCreadoToDo, dueDateToDo: this.actividad.dueDateToDo, statusToDo: this.actividad.statusToDo, idUsuario: this.actividad.idUsuario[index], idReunion: this.idReunion, idReunionIniciada: this.idReunionIniciada }
        await this._reunion.crearActividad(data).toPromise()
      }
      this.actividad= { idToDo: '', tituloToDo: '', detallesToDo: '', dueDateToDo: '', statusToDo: '', idUsuario: [], idReunion: '', idReunionIniciada: '' }

      Swal.fire({
        icon: 'success',
        title: 'Se creó la actividad con éxito',
        showConfirmButton: false,
        timer: 1500
      });
      this.cerrarModal()
    }

  }

  resolver(data: any) {
    data.statusIssue = 'Resuelto'
    this._reunion.actualizarStatusProblema(data).subscribe((resp: any) => {
      this.obtenerProblemas()
    }, err => {
      console.log(err);

    })
  }

  async enumerar(index: any) {
    const problema = this.problemas[index];
    try {
      if (problema.numeroIssue == null) {
        if (this.indiceSeleccionado < 3) {
          this.indiceSeleccionado++;
          const data = { numeroIssue: this.indiceSeleccionado, idIssue: problema.idIssue };
          await this._reunion.numerarProblema(data).toPromise();
        } else {
          Swal.fire({
            icon: 'error',
            title: '¡Error!',
            text: 'Primero resuelve los problemas anteriores.',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar'
          });
        }
      } else if (problema.numeroIssue == this.indiceSeleccionado) {
        const data = { numeroIssue: null, idIssue: problema.idIssue };
        await this._reunion.quitarNumeroProblema(data).toPromise();
      } else if (problema.numeroIssue != null && problema.numeroIssue != this.indiceSeleccionado) {
        for (let k = 0; k < this.problemas.length; k++) {
          if (problema.numeroIssue == 1) {
            const aux = +this.problemas[k].numeroIssue - 1;
            const data = { numeroIssue: (aux < 1) ? null : aux, idIssue: this.problemas[k].idIssue };
            await this._reunion.quitarNumeroProblema(data).toPromise();
          }
          if (problema.numeroIssue == 2) {
            const dataQuitar = { numeroIssue: null, idIssue: problema.idIssue };
            await this._reunion.quitarNumeroProblema(dataQuitar).toPromise();
            for (let j = 0; j < this.problemas.length; j++) {
              if (this.problemas[j].numeroIssue == 3) {
                const dataModificar = { numeroIssue: 2, idIssue: this.problemas[j].idIssue };
                await this._reunion.quitarNumeroProblema(dataModificar).toPromise();
              }
            }
          }
        }
      }
    } catch (error) {
      console.error('Error en la operación:', error);
    } finally {
      this.obtenerProblemas();
    }
  }


  cerrarModal() {
    this._modal.dismissAll();
  }
}

