import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';

@Component({
  selector: 'app-rompehielos',
  templateUrl: './rompehielos.component.html',
  styleUrls: ['./rompehielos.component.css']
})
export class RompehielosComponent {
  paginas: any
  idReunion: any
  idReunionIniciada: any

  participantes: any
  respDatosReunion: any

  constructor(private _service: ApirestService, private _reunion: ReunionesService, private _router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.obtenerDetalles()
  }

  async obtenerDetalles() {
    this.route.params.subscribe(async params => {
      const idReunionString = params['idReunionIniciada'];
      const idNumero = parseInt(idReunionString.match(/\d+/)[0]);
      this.idReunionIniciada = 'SR' + idNumero
      const auxiliarID: any = await this._reunion.obtenerReunionDeIniciada(this.idReunionIniciada).toPromise()
      this.idReunion = auxiliarID[0].idReunion
      this.obtenerReuniones()
    });
  }

  async obtenerReuniones() {
    try {
      this.respDatosReunion = await this._reunion.obtenerDatosReunion(this.idReunion).toPromise();
      this.participantes = this.respDatosReunion.map((datos: any) => {
        return {
          nombre: datos.nombreUsuario,
          pfp: datos.imagenUsuario,
          apellido: datos.apellidoUsuario,
          email: datos.emailUsuario
        };
      });
    } catch (error) {
      console.log(error);
    }
  }
}
