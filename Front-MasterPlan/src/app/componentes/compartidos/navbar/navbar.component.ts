import { HttpClient } from '@angular/common/http';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import jwt_decode from "jwt-decode";
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';
import Swal from 'sweetalert2';
import * as ClassicEditor from 'ckeditor5-custom/build/ckeditor';
import { AuthService } from 'src/app/servicios/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  nombreUsuario: String = ''
  apellidoUsuario: String = ''
  roleUsuario: String = ''
  pfp: any
  emailUsuario: any

  constructor(private _service: ApirestService, private _reuniones: ReunionesService, private _modal: NgbModal, private http: HttpClient, private authService: AuthService) { }
  @ViewChild('agregarAsuntoModal', { static: false }) agregarAsuntoModal!: TemplateRef<any>;
  @ViewChild('crearProblemaModal', { static: false }) agregarProblemaModal!: TemplateRef<any>;
  @ViewChild('crearActividadModal', { static: false }) agregarActividadModal!: TemplateRef<any>;
  public Editor: any = ClassicEditor;

  reuniones: any
  idUsuario: any
  asunto = { idHeadline: '', nombreHeadline: '', detallesHeadline: '', statusHeadline: '', idUsuario: '', idReunion: [], idReunionIniciada: null };
  problema = { idIssue: '', nombreIssue: '', detallesIssue: '', fechaIssue: '', statusIssue: '', idUsuario: '', idReunion: [], idReunionIniciada: null }
  actividad: any = { idToDo: '', tituloToDo: '', detallesToDo: '', fechaCreadoToDo: '', dueDateToDo: '', statusToDo: '', idUsuario: '', idReunion: [], idReunionIniciada: '' }


  ngOnInit(): void {
    this.obtenerSesion()
  }

  async obtenerSesion() {
    const token: any = localStorage.getItem('token');
    const tokenPayload: any = jwt_decode(token);
    this.roleUsuario = tokenPayload.roleUsuario;
    this.emailUsuario = tokenPayload.emailUsuario;
    const resp: any = await this._service.obtenerUsuarioSesion(tokenPayload.emailUsuario).toPromise();

    this.nombreUsuario = resp[0].nombreUsuario;
    this.apellidoUsuario = resp[0].apellidoUsuario;
    this.pfp = resp[0].imagenUsuario;
    const reader = new FileReader();
    reader.onload = (this.pfp);
    const aux = this.nombreUsuario.split(" ");
    if (aux.length > 1) {
      this.nombreUsuario = aux[0];
    }
    this.idUsuario = await this._service.obtenerIDUsuario(tokenPayload.emailUsuario).toPromise();
    const idUsuario = this.idUsuario[0].idUsuario;
    this.idUsuario = idUsuario
    const respReuniones = await this._reuniones.obtenerIDReunion(idUsuario).toPromise();
    this.reuniones = respReuniones;
    function compare(a: any, b: any): number {
      return a.idReunion.split("R")[1] - b.idReunion.split("R")[1];
    }
    this.reuniones.sort(compare);
    for (let reus = 0; reus < this.reuniones.length; reus++) {
      try {
        const respDatosReunion: any = await this._reuniones.obtenerDatosReunion(this.reuniones[reus].idReunion).toPromise();
        if (respDatosReunion && respDatosReunion.length > 0) {
          this.reuniones[reus] = {
            idReunion: this.reuniones[reus].idReunion,
            statusReunion: respDatosReunion[0].statusReunion || 'Valor predeterminado',
            nombreReunion: respDatosReunion[0].nombreReunion,
          };
        } else {
          console.error('El valor de respDatosReunion no está definido o es un array vacío.');
        }
      } catch (error) {
        console.log(error);
      }
    }
  }

  abrirNuevoAsuntoModal() {
    this._modal.open(this.agregarAsuntoModal);
  }

  agregarAsunto() {
    this.asunto.idUsuario = this.idUsuario
    if (this.asunto.nombreHeadline == '' || this.asunto.idUsuario == '') {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos.',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this.asunto.statusHeadline = 'Activo'
      for (let index = 0; index < this.asunto.idReunion.length; index++) {
        let data = {
          nombreHeadline: this.asunto.nombreHeadline, detallesHeadline: this.asunto.detallesHeadline, statusHeadline: this.asunto.statusHeadline, idUsuario: this.asunto.idUsuario, idReunion: this.asunto.idReunion[index], idReunionIniciada: null
        };
        this._reuniones.crearAsunto(data).subscribe((resp: any) => {
          Swal.fire({
            icon: 'success',
            title: 'Asunto creado con éxito!',
            showConfirmButton: false,
            timer: 1500
          });
          this.cerrarModal();
        }, (error) => {
          console.log(error);
        })
      }
      this.asunto = {
        idHeadline: '', nombreHeadline: '', detallesHeadline: '', statusHeadline: '', idUsuario: '', idReunion: [], idReunionIniciada: null
      };
    }
  }

  abrirNuevaActividadModal() {
    this._modal.open(this.agregarActividadModal);
  }

  async agregarActividad() {
    this.actividad.idUsuario = this.idUsuario
    if (this.actividad.tituloToDo == '' || this.actividad.dueDateToDo == '') {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos.',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this.actividad.statusToDo = 'Activo'
      for (let index = 0; index < this.actividad.idReunion.length; index++) {
        const data = { tituloToDo: this.actividad.tituloToDo, detallesToDo: this.actividad.detallesToDo, dueDateToDo: this.actividad.dueDateToDo, statusToDo: this.actividad.statusToDo, idUsuario: this.actividad.idUsuario, idReunion: this.actividad.idReunion[index], idReunionIniciada: null }
        await this._reuniones.crearActividad(data).toPromise()
      }
      this.actividad = { idToDo: '', tituloToDo: '', detallesToDo: '', dueDateToDo: '', statusToDo: '', idUsuario: [], idReunion: '', idReunionIniciada: '' }
      Swal.fire({
        icon: 'success',
        title: 'Se creó la actividad con éxito',
        showConfirmButton: false,
        timer: 1500
      });
      this.cerrarModal()
    }
  }

  abrirNuevoProblemaModal() {
    this._modal.open(this.agregarProblemaModal);
  }

  async agregarProblema() {
    this.problema.idUsuario = this.idUsuario
    if (this.problema.nombreIssue == '') {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this.problema.statusIssue = 'Activo'
      let fechaHoy = new Date();
      let fechaSolo = fechaHoy.toISOString().split('T')[0];
      this.problema.fechaIssue = fechaSolo
      for (let index = 0; index < this.problema.idReunion.length; index++) {
        const data = { nombreIssue: this.problema.nombreIssue, detallesIssue: this.problema.detallesIssue, fechaIssue: this.problema.statusIssue, statusIssue: this.problema.statusIssue, idUsuario: this.problema.idUsuario, idReunion: this.problema.idReunion[index], idReunionIniciada: null }
        await this._reuniones.crearProblema(data).toPromise()
      }

      Swal.fire({
        icon: 'success',
        title: 'Problema creado exitosamente!',
        showConfirmButton: false,
        timer: 1500
      })
      this.problema = { idIssue: '', nombreIssue: '', detallesIssue: '', fechaIssue: '', statusIssue: '', idUsuario: '', idReunion: [], idReunionIniciada: null }
      this.cerrarModal()
    }
  }


  cerrarModal() {
    this._modal.dismissAll();
  }

  cerrarSesion(){
    this.authService.logout()
  }

}
